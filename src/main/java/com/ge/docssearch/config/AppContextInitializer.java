package com.ge.docssearch.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import com.ge.docssearch.elasticsearch.ElasticSearchClient;

/**
 * @author 212601943
 *
 */
public class AppContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

	private static final Logger OUT = LoggerFactory.getLogger(AppContextInitializer.class);

	@Override
	public void initialize(ConfigurableApplicationContext arg0) {
		try {
			ElasticSearchClient.INSTANCE.doInit();
		} catch (Exception e) {
			OUT.error("Exception", e);
		}
	}

}
