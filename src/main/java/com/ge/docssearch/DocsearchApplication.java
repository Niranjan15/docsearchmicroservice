package com.ge.docssearch;

import javax.servlet.MultipartConfigElement;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import com.ge.docssearch.config.AppContextInitializer;

//@SpringBootApplication(scanBasePackages = "com.ge.docssearch")
//public class DocsearchApplication {
//
//	public static void main(String[] args) {
//		new SpringApplicationBuilder(DocsearchApplication.class).run();
//	}

//}

@SpringBootApplication(scanBasePackages = "com.ge.docssearch")
public class DocsearchApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.initializers(new AppContextInitializer()).sources(DocsearchApplication.class);
	}

	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		factory.setMaxFileSize("1024MB");
		factory.setMaxRequestSize("1024MB");
		return factory.createMultipartConfig();
	}

	// @Bean
	// public CommonsMultipartResolver multipartResolver() {
	// CommonsMultipartResolver commonsMultipartResolver = new
	// CommonsMultipartResolver();
	// commonsMultipartResolver.setMaxUploadSize(500000);
	// return commonsMultipartResolver;
	// }
}
//
// public static void main(String[] args) {
//// new SpringApplicationBuilder(DocsearchApplication.class).initializers(new
// AppContextInitializer()).run();
// SpringApplication.run(DocsearchApplication.class, args);
// }
// }
