package com.ge.docssearch.vo;

import java.io.Serializable;

public class SearchVo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String searchText;
	private int from;
	private int size;

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
