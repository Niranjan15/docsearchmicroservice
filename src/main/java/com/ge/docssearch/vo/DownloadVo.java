package com.ge.docssearch.vo;

import java.io.Serializable;

/**
 * @author 212601943
 *
 */
public class DownloadVo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
