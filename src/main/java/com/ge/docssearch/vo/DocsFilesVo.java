package com.ge.docssearch.vo;

import java.io.Serializable;

import com.ge.docssearch.constant.ESConstants;

/**
 * @author 212601943
 *
 */
public class DocsFilesVo extends ESCommonFields implements Serializable {

	private static final long serialVersionUID = 1L;

	private String fileName;
	private byte[] file;
	private String contentType;

	public DocsFilesVo() {
	}

	/**
	 * @param fileName
	 * @param file
	 * @param contentType
	 */
	public DocsFilesVo(String fileName, byte[] file, String contentType) {
		this.fileName = fileName;
		this.file = file;
		this.id = fileName;
		this.TYPE = ESConstants.ES_TYPE_DOCS;
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
