package com.ge.docssearch.vo;

import java.io.Serializable;

import com.ge.docssearch.constant.ESConstants;

/**
 * @author 212601943
 *
 */
public class DocsContentVo extends ESCommonFields implements Serializable {

	private static final long serialVersionUID = 1L;

	private String fileName;
	private String fileContent;

	/**
	 * 
	 */
	public DocsContentVo() {

	}

	/**
	 * @param fileName
	 * @param fileContent
	 */
	public DocsContentVo(String fileName, String fileContent) {
		this.fileName = fileName;
		this.fileContent = fileContent;
		this.id = fileName;
		this.TYPE = ESConstants.ES_TYPE_DOC_CONTENT;
	}

	/**
	 * @param searchText
	 * @param type
	 */
	public DocsContentVo(String searchText, String type, int from, int size) {
		this.TYPE = type;
		this.scriptName = ESConstants.ES_CONTENT_SEARCH_SCRIPT;
		this.params.put("searchText", searchText);
		this.params.put("from", from);
		this.params.put("size", size);
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileContent() {
		return fileContent;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}

}
