package com.ge.docssearch.vo;

import java.io.Serializable;

public class CommonResultVo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String status;
	private Object result;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
}
