package com.ge.docssearch.vo;

import java.io.Serializable;

/**
 * @author 212601943
 *
 */
public class FileResultVo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String fileName;
	private String fileContent;
	private String contentType;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileContent() {
		return fileContent;
	}

	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

}
