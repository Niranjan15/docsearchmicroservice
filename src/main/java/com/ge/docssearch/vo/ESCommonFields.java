package com.ge.docssearch.vo;

import java.util.HashMap;
import java.util.Map;

import com.ge.docssearch.constant.ESConstants;

public class ESCommonFields {

	protected String INDEX = ESConstants.ES_INDEX;
	protected String TYPE;
	protected String id;
	protected Map<String, Object> params = new HashMap<>();
	protected String scriptName;

	public String getId() {
		return id;
	}

	public String getIndex() {
		return INDEX;
	}

	public String getType() {
		return TYPE;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public String getScriptName() {
		return scriptName;
	}

}
