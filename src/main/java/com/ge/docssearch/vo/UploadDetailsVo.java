package com.ge.docssearch.vo;

import java.io.Serializable;

import com.ge.docssearch.constant.ESConstants;

/**
 * @author 212601943
 *
 */
public class UploadDetailsVo extends ESCommonFields implements Serializable {

	private static final long serialVersionUID = 1L;

	private long uploadedAt;
	private String uploadedBy;
	private long size;

	public UploadDetailsVo(String fileName, String uploadedBy, long size) {
		this.TYPE = ESConstants.ES_TYPE_UPLOAD_DETAILS;
		this.id = fileName;
		this.uploadedBy = uploadedBy;
		this.size = size;
		this.uploadedAt = System.currentTimeMillis();
	}

	public String getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getUploadedAt() {
		return uploadedAt;
	}

	public void setUploadedAt(long uploadedAt) {
		this.uploadedAt = uploadedAt;
	}

}
