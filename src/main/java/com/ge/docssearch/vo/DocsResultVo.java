package com.ge.docssearch.vo;

import java.io.Serializable;

/**
 * @author 212601943
 *
 */
public class DocsResultVo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String fileName;
	private String phrase;

	/**
	 * @param fileName
	 * @param phrase
	 */
	public DocsResultVo(String id, String fileName, String phrase) {
		this.id = id;
		this.fileName = fileName;
		this.phrase = phrase;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPhrase() {
		return phrase;
	}

	public void setPhrase(String phrase) {
		this.phrase = phrase;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
