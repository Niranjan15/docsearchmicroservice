package com.ge.docssearch.service;

import com.ge.docssearch.vo.FileResultVo;

/**
 * @author 212601943
 *
 */
public interface DocsDownloadService {

	/**
	 * @param id
	 * @return
	 */
	FileResultVo doDownload(String id) throws Exception;
}
