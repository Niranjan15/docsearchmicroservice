package com.ge.docssearch.service;

import java.io.File;
import java.util.List;

/**
 * @author 212601943
 *
 */
public interface DocsUploadService {

	/**
	 * This method can be used to upload single file to elastic search
	 * 
	 * @param file
	 * @param uploadedBy
	 * @throws Exception
	 */
	void upload(File file, String uploadedBy) throws Exception;

	/**
	 * This method can be used to upload multiple files to elastic search
	 * 
	 * @param files
	 * @param uploadedBy
	 * @throws Exception
	 */
	void bulkUpload(List<File> files, String uploadedBy) throws Exception;
}
