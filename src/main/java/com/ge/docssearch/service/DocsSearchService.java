package com.ge.docssearch.service;

import java.util.List;

import com.ge.docssearch.vo.DocsResultVo;

/**
 * @author 212601943
 *
 */
public interface DocsSearchService {

	/**
	 * @param searchText
	 * @param type
	 * @param from
	 * @param size
	 * @return
	 * @throws Exception
	 */
	List<DocsResultVo> doSearch(String searchText, String type, int from, int size) throws Exception;
}
