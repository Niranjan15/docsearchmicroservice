package com.ge.docssearch.serviceimpl;

import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import com.ge.docssearch.elasticsearch.ElasticSearchServiceImpl;
import com.ge.docssearch.service.DocsDownloadService;
import com.ge.docssearch.vo.DocsFilesVo;
import com.ge.docssearch.vo.FileResultVo;

/**
 * @author 212601943
 *
 */
@Service
public class DocsDownloadServiceImpl implements DocsDownloadService {

	@Override
	public FileResultVo doDownload(String id) throws Exception {
		DocsFilesVo findById = new ElasticSearchServiceImpl<DocsFilesVo>().findById(new DocsFilesVo(id, null, null));
		FileResultVo fileResultVo = null;
		if (findById != null) {
			fileResultVo = new FileResultVo();
			fileResultVo.setFileContent(Base64Utils.encodeToString(findById.getFile()));
			fileResultVo.setFileName(findById.getFileName());
			fileResultVo.setContentType(findById.getContentType());
			return fileResultVo;
		}
		return null;
	}

}
