package com.ge.docssearch.serviceimpl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.xmlbeans.XmlException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.ge.docssearch.elasticsearch.ElasticSearchServiceImpl;
import com.ge.docssearch.service.DocsUploadService;
import com.ge.docssearch.utils.OLEOrOOXMLFileUtils;
import com.ge.docssearch.vo.DocsContentVo;
import com.ge.docssearch.vo.DocsFilesVo;
import com.ge.docssearch.vo.UploadDetailsVo;
import com.google.common.io.Files;

/**
 * @author 212601943
 *
 */
@Service
public class DocsUploadServiceImpl implements DocsUploadService {

	private static final Logger OUT = LoggerFactory.getLogger(DocsUploadServiceImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ge.docssearch.service.DocsUploadService#upload(java.io.File)
	 */
	@Override
	public void upload(File file, String uploadedBy) throws Exception {
		DocsContentVo docsContent = getDocsContent(file);
		DocsFilesVo docsFiles = getDocsFiles(file);
		UploadDetailsVo uploadDetails = getUploadSummary(file, uploadedBy);
		// Content String
		new ElasticSearchServiceImpl<DocsContentVo>().insert(docsContent);
		// File
		new ElasticSearchServiceImpl<DocsFilesVo>().insert(docsFiles);
		// Upload details
		new ElasticSearchServiceImpl<UploadDetailsVo>().insert(uploadDetails);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ge.docssearch.service.DocsUploadService#bulkUpload(java.io.File[])
	 */
	@Override
	public void bulkUpload(List<File> files, String uploadedBy) throws Exception {
		List<DocsContentVo> contentList = new ArrayList<>();
		List<DocsFilesVo> filesList = new ArrayList<>();
		List<UploadDetailsVo> uploadDetails = new ArrayList<>();
		files.parallelStream()
				.forEach(file -> doPopFileNContent(file, contentList, filesList, uploadDetails, uploadedBy));
		// Content String
		new ElasticSearchServiceImpl<DocsContentVo>().bulkInsert(contentList);
		// File
		new ElasticSearchServiceImpl<DocsFilesVo>().bulkInsert(filesList);
		// Upload details
		new ElasticSearchServiceImpl<UploadDetailsVo>().bulkInsert(uploadDetails);
	}

	/**
	 * @param file
	 * @param contentList
	 * @param filesList
	 * @param uploadDetails
	 * @param uploadedBy
	 * @throws Exception
	 */
	private void doPopFileNContent(File file, List<DocsContentVo> contentList, List<DocsFilesVo> filesList,
			List<UploadDetailsVo> uploadDetails, String uploadedBy) {
		try {
			switch (FilenameUtils.getExtension(file.getName())) {
			case "doc":
			case "docx":
			case "xls":
			case "xlsx":
			case "ppt":
			case "pptx":
				contentList.add(getDocsContent(file));
				filesList.add(getDocsFiles(file));
				uploadDetails.add(getUploadSummary(file, uploadedBy));
				break;
			default:
				OUT.warn("Unknown file type received {}", FilenameUtils.getExtension(file.getName()));
				break;
			}
		} catch (Exception e) {
			OUT.error("Exception", e);
		}
	}

	/**
	 * @param file
	 * @return
	 * @throws IOException
	 */
	private DocsFilesVo getDocsFiles(File file) throws IOException {
		return new DocsFilesVo(file.getName(), Files.toByteArray(file), java.nio.file.Files.probeContentType(file.toPath()));
	}

	/**
	 * @param file
	 * @return
	 * @throws IOException
	 * @throws OpenXML4JException
	 * @throws XmlException
	 */
	private DocsContentVo getDocsContent(File file) throws IOException, OpenXML4JException, XmlException {
		return new DocsContentVo(file.getName(), new OLEOrOOXMLFileUtils().getContent(file));
	}

	/**
	 * @param file
	 * @param uploadedBy
	 * @return
	 */
	private UploadDetailsVo getUploadSummary(File file, String uploadedBy) {
		return new UploadDetailsVo(file.getName(), uploadedBy, getFileSize(file.length()));
	}

	/**
	 * @param byteSize
	 * @return
	 */
	private long getFileSize(long byteSize) {
		return (byteSize / 1024);
	}
}
