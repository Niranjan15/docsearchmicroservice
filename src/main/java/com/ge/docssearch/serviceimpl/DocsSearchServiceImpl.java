package com.ge.docssearch.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.ge.docssearch.elasticsearch.ElasticSearchServiceImpl;
import com.ge.docssearch.service.DocsSearchService;
import com.ge.docssearch.vo.DocsContentVo;
import com.ge.docssearch.vo.DocsResultVo;

/**
 * @author 212601943
 *
 */
@Service
public class DocsSearchServiceImpl implements DocsSearchService {

	private static final Logger OUT = LoggerFactory.getLogger(DocsSearchServiceImpl.class);

	private static final String B_START = "<b>";
	private static final String B_END = "</b>";
	private static final int PHRASE_SIZE = 150;

	@Override
	public List<DocsResultVo> doSearch(String searchText, String type, int from, int size) throws Exception {
		ElasticSearchServiceImpl<DocsContentVo> elasticSearchServiceImpl = new ElasticSearchServiceImpl<DocsContentVo>();
		DocsContentVo docsContentVo = new DocsContentVo(searchText, type, from, size);
		elasticSearchServiceImpl.count(docsContentVo);
		List<DocsContentVo> searchResult = elasticSearchServiceImpl.searchTemplateFile(docsContentVo);
		List<DocsResultVo> resultList = null;
		if (!CollectionUtils.isEmpty(searchResult)) {
			resultList = new ArrayList<>(searchResult.size());
			for (DocsContentVo docsContentVO : searchResult) {
				resultList.add(new DocsResultVo(docsContentVO.getId(), docsContentVO.getFileName(),
						getPhrases(docsContentVO.getFileContent(), searchText)));
			}
		}
		return resultList;
	}

	private String getPhrases(String fileContent, String searchText) {
		StringBuilder stringBuilder = new StringBuilder();
		for (String searchWorld : searchText.split(" ")) {
			stringBuilder.append(getPhrase(fileContent, searchWorld)).append("...");
		}
		return stringBuilder.toString();
	}

	/**
	 * @param fileContent
	 * @param searchText
	 * @param searchWorld
	 * @return
	 */
	private String getPhrase(String fileContent, String searchWorld) {
		StringBuilder builder = new StringBuilder("");
		int firstWindex = new String(fileContent.toLowerCase()).indexOf(searchWorld.toLowerCase());
		if (firstWindex != -1) {
			int totalFileLength = fileContent.length();
			int beginIndex = getBeginIndex(firstWindex);
			int endIndex = getEndIndex(firstWindex, totalFileLength);
			String substring = fileContent.substring(beginIndex, endIndex);
			beginIndex = substring.indexOf(" ");
			endIndex = substring.lastIndexOf(" ");
			String[] split = substring.substring(beginIndex, endIndex).split(searchWorld);
			builder.append(split[0]);
			builder.append(" <b>").append(searchWorld).append("</b> ");
			if (split.length > 1) {
				builder.append(split[1]);
			}
		}
		return builder.toString();
	}

	// /**
	// * @param fileContent
	// * @param searchText
	// * @return
	// */
	// private String getPhrase(String fileContent, String searchText) {
	// String[] searchWorlds = searchText.split(" ");
	// int firstWindex = new
	// String(fileContent.toLowerCase()).indexOf(searchWorlds[0].toLowerCase());
	// int totalFileLength = fileContent.length();
	// int beginIndex = getBeginIndex(firstWindex);
	// int endIndex = getEndIndex(firstWindex, totalFileLength);
	// String substring = fileContent.substring(beginIndex, endIndex);
	// beginIndex = substring.indexOf(" ");
	// endIndex = substring.lastIndexOf(" ");
	// return boldMatch(searchWorlds, substring.substring(beginIndex,
	// endIndex));
	// }
	//
	// /**
	// * @param searchWorlds
	// * @param substring
	// * @return
	// */
	// private String boldMatch(String[] searchWorlds, String substring) {
	// try {
	// StringBuilder stringBuilder = new StringBuilder(substring);
	// String lowerCaseContent = new String(substring.toLowerCase());
	// for (String searchWorld : searchWorlds) {
	// if (lowerCaseContent.contains(searchWorld.toLowerCase())) {
	// int indexOf = lowerCaseContent.indexOf(searchWorld.toLowerCase());
	// stringBuilder.append(B_START, indexOf, indexOf);
	// int indexOf2 = lowerCaseContent.substring(indexOf,
	// lowerCaseContent.length()).indexOf(" ");
	// stringBuilder.append(B_END, indexOf2, indexOf2);
	// }
	// }
	// } catch (Exception e) {
	// OUT.error("Exception", e);
	// }
	// return substring;
	// }

	/**
	 * @param firstWindex
	 * @param totalFileLength
	 * @return
	 */
	private int getEndIndex(int firstWindex, int totalFileLength) {
		int lastIndex = totalFileLength;
		if ((firstWindex + PHRASE_SIZE) < totalFileLength) {
			lastIndex = firstWindex + PHRASE_SIZE;
		}
		return lastIndex;
	}

	/**
	 * @param firstWindex
	 * @return
	 */
	private int getBeginIndex(int firstWindex) {
		int startIndex = 0;
		if (firstWindex > PHRASE_SIZE) {
			startIndex = firstWindex - PHRASE_SIZE;
		}
		return startIndex;
	}

}
