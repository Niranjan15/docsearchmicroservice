package com.ge.docssearch.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ge.docssearch.constant.AppConstants;
import com.ge.docssearch.constant.URIConst;
import com.ge.docssearch.service.DocsUploadService;
import com.ge.docssearch.vo.CommonResultVo;

@RestController
public class DocsUploadController {

	private static final Logger OUT = LoggerFactory.getLogger(DocsUploadController.class);

	@Autowired
	DocsUploadService docsUploadService;

	@PostMapping(URIConst.BULK_UPLOAD)
	public CommonResultVo uploadFiles(@RequestParam("file") MultipartFile[] files) {
		OUT.info("Files length"+files.length);
		CommonResultVo commonResultVo = new CommonResultVo();
		try {
			List<File> fileList = new ArrayList<>();
			for (int i = 0; i < files.length; i++) {
				fileList.add(getFileFromMultipart(files[i]));
			}
			docsUploadService.bulkUpload(fileList, "212601943");
			cleanUp(fileList.toArray(new File[] {}));
			commonResultVo.setStatus(AppConstants.SUCCESS);
		} catch (Exception e) {
			OUT.error("Exception", e);
			commonResultVo.setStatus(AppConstants.FAILED);
		}
		return commonResultVo;
	}

	@PostMapping(URIConst.UPLOAD)
	public CommonResultVo uploadFile(@RequestParam("file") MultipartFile file) {
		OUT.info("Files length"+file.getOriginalFilename());
		CommonResultVo commonResultVo = new CommonResultVo();
		try {
			File fileFromMultipart = getFileFromMultipart(file);
			docsUploadService.upload(fileFromMultipart, "212601943");
			cleanUp(fileFromMultipart);
			commonResultVo.setStatus(AppConstants.SUCCESS);
		} catch (Exception e) {
			OUT.error("Exception", e);
			commonResultVo.setStatus(AppConstants.FAILED);
		}
		return commonResultVo;
	}

	/**
	 * @param file
	 * @return
	 * @throws IOException
	 */
	private File getFileFromMultipart(MultipartFile file) throws IOException {
		File tempDir = new File(AppConstants.TMP_DIR);
		if (!tempDir.exists()) {
			tempDir.mkdirs();
		}
		File contentFile = new File(AppConstants.TMP_DIR + File.separatorChar + file.getOriginalFilename());
		file.transferTo(contentFile);
		return contentFile;
	}

	/**
	 * @param files
	 */
	private void cleanUp(File... files) {
		for (File file : files) {
			if (file.exists()) {
				file.delete();
			}
		}
	}
}
