package com.ge.docssearch.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ge.docssearch.constant.AppConstants;
import com.ge.docssearch.constant.URIConst;
import com.ge.docssearch.service.DocsDownloadService;
import com.ge.docssearch.vo.CommonResultVo;
import com.ge.docssearch.vo.DownloadVo;

@RestController
public class DocsDownloadController {

	private static final Logger OUT = LoggerFactory.getLogger(DocsDownloadController.class);

	@Autowired
	DocsDownloadService docsDownloadService;

	@PostMapping(URIConst.DOWNLOAD)
	public CommonResultVo doDownloadFile(@RequestBody DownloadVo downloadVo) {
		CommonResultVo commonResultVo = new CommonResultVo();
		try {
			commonResultVo.setResult(docsDownloadService.doDownload(downloadVo.getId()));
			commonResultVo.setStatus(AppConstants.SUCCESS);
		} catch (Exception e) {
			commonResultVo.setStatus(AppConstants.FAILED);
			OUT.error("Exception", e);
		}
		return commonResultVo;
	}
}
