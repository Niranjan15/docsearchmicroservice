package com.ge.docssearch.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ge.docssearch.constant.AppConstants;
import com.ge.docssearch.constant.ESConstants;
import com.ge.docssearch.constant.URIConst;
import com.ge.docssearch.service.DocsSearchService;
import com.ge.docssearch.vo.CommonResultVo;
import com.ge.docssearch.vo.SearchVo;

@RestController
public class DocsSearchController {

	private static final Logger OUT = LoggerFactory.getLogger(DocsSearchController.class);

	@Autowired
	DocsSearchService docsSearchService;

	@PostMapping(URIConst.SEARCH)
	public CommonResultVo doSearch(@RequestBody SearchVo searchVo) {
		CommonResultVo commonResultVo = new CommonResultVo();
		try {
			commonResultVo.setResult(docsSearchService.doSearch(searchVo.getSearchText(),
					ESConstants.ES_TYPE_DOC_CONTENT, searchVo.getFrom(), searchVo.getSize()));
			commonResultVo.setStatus(AppConstants.SUCCESS);
		} catch (Exception e) {
			OUT.error("Exception", e);
			commonResultVo.setStatus(AppConstants.FAILED);
		}
		return commonResultVo;
	}
}
