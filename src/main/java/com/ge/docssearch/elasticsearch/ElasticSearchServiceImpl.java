package com.ge.docssearch.elasticsearch;

import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.script.mustache.SearchTemplateRequestBuilder;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ge.docssearch.utils.GEObjectMapper;
import com.ge.docssearch.vo.DocsContentVo;
import com.ge.docssearch.vo.ESCommonFields;

public class ElasticSearchServiceImpl<D extends ESCommonFields> implements ElasticSearchService<D> {

	private static final Logger OUT = LoggerFactory.getLogger(ElasticSearchServiceImpl.class);

	@Override
	public void insert(D document) throws Exception {
		ElasticSearchClient.INSTANCE.getClient().prepareIndex(document.getIndex(), document.getType(), document.getId())
				.setSource(new GEObjectMapper().getJsonString(document)).get();
	}

	@Override
	public void bulkInsert(List<D> documents) {
		TransportClient client = ElasticSearchClient.INSTANCE.getClient();
		BulkRequestBuilder bulkRequest = client.prepareBulk();
		documents.parallelStream().forEach(doc -> {
			try {
				GEObjectMapper objectMapper = new GEObjectMapper();
				bulkRequest.add(client.prepareIndex(doc.getIndex(), doc.getType(), doc.getId())
						.setSource(objectMapper.getJsonString(doc)));
			} catch (JsonProcessingException e) {
				OUT.error("Exception", e);
			}
		});
		BulkResponse bulkResponse = bulkRequest.get();
		if (bulkResponse.hasFailures()) {
			OUT.error("Elastic search bulk insert failed : {}", bulkResponse.getItems());
		}
	}

	@Override
	public List<D> searchTemplateFile(D searchDetails) throws Exception {
		SearchResponse sr = new SearchTemplateRequestBuilder(ElasticSearchClient.INSTANCE.getClient())
				.setScript(searchDetails.getScriptName()).setScriptType(ScriptType.FILE)
				.setScriptParams(searchDetails.getParams()).setRequest(new SearchRequest()).get().getResponse();
		SearchHit[] hits = sr.getHits().getHits();
		List<D> resultList = null;
		if (hits.length > 0) {
			resultList = new ArrayList<>(hits.length);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			for (SearchHit searchHit : hits) {
				resultList.add((D) mapper.readValue(searchHit.getSourceAsString(), searchDetails.getClass()));
			}
		}
		return resultList;
	}

	@Override
	public D findById(D doc) throws Exception {
		GetResponse getResponse = ElasticSearchClient.INSTANCE.getClient()
				.prepareGet(doc.getIndex(), doc.getType(), doc.getId()).get();
		if (!getResponse.isSourceEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			return (D) mapper.readValue(getResponse.getSourceAsString(), doc.getClass());
		}
		return null;
	}

	@Override
	public long count(DocsContentVo docsContentVo) {
		//ElasticSearchClient.INSTANCE.getClient().prepare
		return 0;
	}

	@Override
	public List<?> searchTemplateInline(D searchDetails) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
