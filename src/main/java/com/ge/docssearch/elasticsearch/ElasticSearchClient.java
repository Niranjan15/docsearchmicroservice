package com.ge.docssearch.elasticsearch;

import java.net.InetAddress;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 212601943
 *
 */
public enum ElasticSearchClient {

	INSTANCE;
	private static final Logger OUT = LoggerFactory.getLogger(ElasticSearchClient.class);
	private TransportClient client = null;

	/**
	 * @throws Exception
	 */
	public void doInit() throws Exception {
		if (client == null) {
			OUT.debug("****************	Elasticsearch initialization started	**************");
			client = new PreBuiltTransportClient(Settings.EMPTY)
					.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300));
			OUT.debug("****************	Elasticsearch initialization compleated	**************");
		}
	}

	/**
	 * @throws Exception
	 */
	public void doDestroy() throws Exception {
		if (client != null) {
			client.close();
		}
	}

	/**
	 * @return
	 */
	public TransportClient getClient() {
		return client;
	}
}
