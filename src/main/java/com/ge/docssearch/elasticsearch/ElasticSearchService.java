package com.ge.docssearch.elasticsearch;

import java.util.List;

import com.ge.docssearch.vo.DocsContentVo;
import com.ge.docssearch.vo.ESCommonFields;

/**
 * @author 212601943
 *
 * @param <D>
 */
public interface ElasticSearchService<D extends ESCommonFields> {

	/**
	 * @param document
	 * @throws Exception
	 */
	void insert(D document) throws Exception;

	/**
	 * @param documents
	 */
	void bulkInsert(List<D> documents);

	/**
	 * @param searchDetails
	 * @return
	 * @throws Exception
	 */
	List<?> searchTemplateFile(D searchDetails) throws Exception;

	/**
	 * @param doc
	 * @return
	 * @throws Exception
	 */
	D findById(D doc) throws Exception;

	/**
	 * @param docsContentVo
	 * @return
	 */
	long count(DocsContentVo docsContentVo);
	
	/**
	 * @param searchDetails
	 * @return
	 * @throws Exception
	 */
	List<?> searchTemplateInline(D searchDetails) throws Exception;

}
