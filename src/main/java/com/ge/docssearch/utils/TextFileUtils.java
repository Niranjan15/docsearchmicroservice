package com.ge.docssearch.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author 212601943
 *
 */
public class TextFileUtils {

	/**
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public String getContent(File file) throws FileNotFoundException, IOException {
		try (BufferedReader inputStream = new BufferedReader(new FileReader(file))) {
			StringBuilder fileContent = new StringBuilder();
			String readLine = null;
			while ((readLine = inputStream.readLine()) != null) {
				fileContent.append(readLine);
			}
			return fileContent.toString();
		}

	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
		System.out.println(new TextFileUtils().getContent(new File(
				"C:\\Niranjan\\Workspace\\docsearch\\src\\main\\java\\com\\ge\\docssearch\\utils\\new joinees.docx")));
	}

}
