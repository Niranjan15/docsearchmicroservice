package com.ge.docssearch.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.POITextExtractor;
import org.apache.poi.extractor.ExtractorFactory;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.xmlbeans.XmlException;

/**
 * @author 212601943
 * 
 *         Supports OLE2 file and OOXML file
 */
public class OLEOrOOXMLFileUtils {

	public String getContent(File file) throws IOException, OpenXML4JException, XmlException {
		POITextExtractor extractor = ExtractorFactory.createExtractor(file);
		return extractor.getText();
	}

	public static void main(String[] args) throws FileNotFoundException, IOException, OpenXML4JException, XmlException {
		System.out.println(new OLEOrOOXMLFileUtils().getContent(new File(
				"C:\\Niranjan\\Workspace\\docsearch\\src\\main\\java\\com\\ge\\docssearch\\utils\\ScottBolteGE.ppt")));
	}
}
