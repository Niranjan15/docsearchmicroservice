package com.ge.docssearch.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author 212601943
 *
 */
public class GEObjectMapper {

	/**
	 * @param obj
	 * @return
	 * @throws JsonProcessingException
	 */
	public String getJsonString(Object obj) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(obj);
	}
}
