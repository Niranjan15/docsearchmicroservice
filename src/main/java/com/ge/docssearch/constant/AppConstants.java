package com.ge.docssearch.constant;

/**
 * @author 212601943
 *
 */
public interface AppConstants {
	String TMP_DIR = System.getProperty("java.io.tmpdir");
	String SUCCESS = "success";
	String FAILED = "failed";
}
