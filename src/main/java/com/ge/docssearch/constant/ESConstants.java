package com.ge.docssearch.constant;

/**
 * @author 212601943
 *
 */
public interface ESConstants {

	String ES_INDEX = "gedocs";
	String ES_TYPE_DOC_CONTENT = "docs_content";
	String ES_TYPE_DOCS = "docs";
	String ES_TYPE_UPLOAD_DETAILS = "upload_details";
	// Scripts
	String ES_CONTENT_SEARCH_SCRIPT = "ESContentSearch";
}
