package com.ge.docssearch.constant;

public interface URIConst {

	String SEARCH = "/api/search";
	String DOWNLOAD = "/api/download";
	String UPLOAD = "/api/upload";
	String BULK_UPLOAD = "/api/bulkUpload";
}
